from validate import validate_number

def create_account(curr_bal):
    if curr_bal == -1:
        initial_deposit = input("How much will your initial depsoit be? ")
        while validate_number(initial_deposit) == False:
            initial_deposit = input("Please enter a decimal number: ")    
        print(f"Thanks!  Your Account is now open with an initial balance of ${float(initial_deposit):.2f}.")
        return initial_deposit
    else:
        print(f"You already have an account silly!  Your balance is ${float(curr_bal):.2f}.")
        return curr_bal
    

def deposit(curr_bal):
    if curr_bal == -1:
        print("You don't have an account yet!  Please open one before trying to deposit money.")
        return curr_bal
    else:
        deposit = input("How much will your deposit be? ")
        while validate_number(deposit) == False:
            deposit = input("Please enter a decimal number: ")    
        curr_bal = float(curr_bal) + float(deposit)
        print(f"Thanks!  Your updated balance is: ${float(curr_bal):.2f}.")
        return curr_bal
    
def withdraw(curr_bal):
    if curr_bal == -1:
        print("You don't have an account yet!  We don't just give away money!")
        return curr_bal
    else:
        withdrawl = input("How much will you withdraw? ")
        while validate_number(withdrawl) == False:
            withdrawl = input("Please enter a decimal number: ")  
        if float(curr_bal) >= float(withdrawl):  
            curr_bal = float(curr_bal) - float(withdrawl)
            print(f"Don't spend it all in one place!  Your updated balance is: ${float(curr_bal):.2f}.")
            return curr_bal
        else:
            print("Sorry!  You don't have that much money!  Maybe donate some plasma")
            return curr_bal
    
def check_balance(curr_bal):
    if curr_bal == -1:
        print("You don't have an account yet!  Why not open one?")
    else:
        print(f"Your balance is: ${float(curr_bal):.2f}.")
 