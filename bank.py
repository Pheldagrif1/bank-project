from account_commands import create_account, deposit, withdraw, check_balance
import datetime


title = '''

  __  __       ____              _    \n\
 |  \/  |     |  _ \            | |   \n\
 | \  / |_   _| |_) | __ _ _ __ | | __\n\
 | |\/| | | | |  _ < / _` | '_ \| |/ /\n\
 | |  | | |_| | |_) | (_| | | | |   < \n\
 |_|  |_|\__, |____/ \__,_|_| |_|_|\_\ \n\
          __/ |                       \n\
         |___/                        \n\
'''
print(title)
print("Welcome to a really basic bank.")
print(f"It is currently {datetime.datetime.now().strftime("%Y-%m-%d %I:%M:%S%p")}")
print("You can:\n\
      C - Create an account\n\
      D - Make a deposit\n\
      W - Withdraw money\n\
      B - Check your balance\n\
      X - Exit")

choice = input("What would you like to do? ")
choice = choice.upper()
balance = -1
while choice != "X":
    if choice == "C":
        balance = create_account(balance)
    elif choice == "D":
        balance = deposit(balance)
    elif choice == "W":
        balance = withdraw(balance)
    elif choice == "B":
        check_balance(balance)
    else: 
        print("That was not a choice.  Lets try that again: ")
        print("You can:\n\
      C - Create an account\n\
      D - Make a deposit\n\
      W - Withdraw money\n\
      B - Check your balance\n\
      X - Exit")
    
    choice = input("What would you like to do? ").upper()


# add you don't have that much money!
#redisplay choices.



